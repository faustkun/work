q = 7
L = []

for i in range(q):
    L.append(2**i)

X = 5
i = 0
# variant 1
while i < len(L):
    if 2 ** X == L[i]:
        print ('at index', i)
        break
    i += 1
else:
    print (X, 'not found')

#variant 2
for C in L:
    if C == 2**X:
        print ('at index', L.index(C))
        break
else:
     print (X, 'not found')

#variant 3
if 2**X in L:
    print(2**X, 'was found at index', L.index(2**X))
else:
    print (X, 'not found')

#variant 4

