def f(x):
    return x ** 3 + 1


def trap(a, b, eps):
    """
    :param a: is start
    :param b: is end
    :param eps: epsilon
    :return: integral of function f(x)
    """
    n, s1, s2 = 2, 0, 0
    while True:
        h = float(b - a)/n
        s2 = float(f(a) + f(b))/2
        x = a + h
        for i in range(n-1):
            s2 += f(x)
            x += h
        s2 *= h
        if abs(s2 - s1)< eps:
            return s2
        s1 = s2
        n *= 2

import time

start = time.time()

for i in range(50):
    trap(-1, 0, 0.000005)

finish = time.time()
print (finish - start)



