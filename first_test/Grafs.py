storage = {'1': [['2', 5], ['3', 3], ['5', 2]],
           '2': [['1', 5], ['3', 1], ['4', 8]],
           '3': [['1', 3], ['2', 1]],
           '4': [['2', 8]],
           '5': [['1', 2]]}
L = [None, []]

def search(storage, start, end, L, temp):

  temp2 = temp[:]
  temp2.append(start)

  if start == end:
      sum = 0
      for iter in range(len(temp2)-1):
          for iter2 in storage[temp2[iter]]:
              if iter2[0] == temp2[iter+1]:
                  sum +=iter2[1]
      if  (sum < L[0]) or (L[0] == None):
          L[0] = sum
          L[1] = temp2
          return 1

  for i in storage[start]:
      if i[0] not in temp2:
         search(storage, i[0], end, L, temp2)
  return 0
#search(storage, '5', '3', L, [])

def get_data(s):
    storage = {}

    for line in open(s):
        q = line.split(' ')
#        q[1] = int(q[1])
        q[2] = q[2].replace('\n', '')
        q[2] = int(q[2])
        if q[0] in storage:
            storage[q[0]].append(q[1:])
        else:
           temp = []
           temp.append(q[1:])
           storage[q[0]] = temp
    return  storage


search(get_data('data.txt'), '5', '10', L, [])
print L

